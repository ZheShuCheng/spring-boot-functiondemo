package com.csc.jar;

import io.xjar.XCryptos;

/**
 * @Author csc
 * @Date 2021-07-13
 * @Description: 该类主要用于存放一些借口地址.或者常量标识
 * @Version 1.0
 */
public class JarEncryption {
    public static void main(String[] args) throws Exception {
        // Spring-Boot Jar包加密
        XCryptos.encryption()
                //待加密jar。你要加密的包的路径+包名称
                .from("H:\\桌面\\加密\\controller.jar")
                .use("io.xjar")
                //忽略内容
                .exclude("/static/**/*")
                .exclude("/META-INF/resources/**/*")
                //加密后输出的jar的地址和jar包名称
                .to("H:\\桌面\\加密\\group_operation-encryption.jar");
        System.out.println("success");
    }
}
