时间: 2021-08-24 09:25:24
创建人:csc
模块说明:   
该项目主要是给您打好的jar包进行加密. 防止别人解压和利用反编译工具来查看源码.

#知识分享
1.本模块主要使用xjar来进行对jar包的加密.
2.xjar加密需要使用go语言环境.下载免安装版本的就可以下载地址(https://studygolang.com/dl)
3.配置GO语言环境变量. 百度搜索一下就有.这里就不在赘叙了.
4. 使用main方法加密后. 会生成xjar.go文件.  此时cmd 执行< go build xjar.go >来进行编译. 
  会生成一个xjar.exe 文件.讲加密后的jar包和xjar.go,xjar.exe 放在同一目录下.
  执行  <xjar java -jar xxx.jar>命令执行.jar包就会执行了
5. xjar加密springBoot项目有一个缺点就是本来运行400-500M内存. 加密后运行jar包会变成1.3G内存.请谨慎使用.
  

