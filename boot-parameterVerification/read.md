
时间:2021-08-23 15:49:45
创建人:csc
说明:  Validator 一般适用于对外提供的接口. 用来对接一下没有页面的数据对接. (比如一些简单的中转服务)
       有页面的不建议使用这个.直接在页面检验就OK了.减轻服务的压力
简介:
    

#知识普及
1. 参数校验用的是spring自带的Spring Validator
  @Null 被注释的元素必须为 null
  
  @NotNull 被注释的元素必须不为 null  应用在基本类型上面
  (不能为null，但可以为empty,没有Size的约束，例如:@NotNull(message=“username !=null”)
   private String username,说明username！=null，but，username=="")
   
  @AssertTrue 被注释的元素必须为 true
  @AssertFalse 被注释的元素必须为 false
  @Min(value) 被注释的元素必须是一个数字，其值必须大于等于指定的最小值
  @Max(value) 被注释的元素必须是一个数字，其值必须小于等于指定的最大值
  @DecimalMin(value) 被注释的元素必须是一个数字，其值必须大于等于指定的最小值
  @DecimalMax(value) 被注释的元素必须是一个数字，其值必须小于等于指定的最大值
  @Size(max=, min=) 被注释的元素的大小必须在指定的范围内
  @Digits(integer, fraction) 被注释的元素必须是一个数字，其值必须在可接受的范围内
  @Past 被注释的元素必须是一个过去的日期
  @Future 被注释的元素必须是一个将来的日期
  @Pattern(regex=,flag=) 被注释的元素必须符合指定的正则表达式
  
2. Hibernate Validator 附加的 constraint(注解):
      @NotBlank 验证字符串非null，且长度必须大于0 (只用于String,不能为null且trim()之后size>0)
      @Email 被注释的元素必须是电子邮箱地址
      @Length(min=,max=) 被注释的字符串的大小必须在指定的范围内
      @NotEmpty 被注释的字符串的必须非空(用在集合类上面)
      @Range(min=,max=,message=) 被注释的元素必须在合适的范围内
      
  
  
