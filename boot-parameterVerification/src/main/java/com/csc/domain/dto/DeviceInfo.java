package com.csc.domain.dto;


import com.csc.bean.Constant;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * 创建人:csc
 * 时间: 2021-07-07 15:15:00
 * 说明: 该类主要用于测试validation参数校验框架
 * 思考:  该类中都加了如下两个参数的不为空校验。
 * 情景1: 如果一个接口对以下字段(ip和序列号)都有不为空的需求那么刚好符合直接使用就OK了
 * 情景2: 如果你的另外一个接口只需要ip/serialNo?怎么办. 因此你不能在创建两个实体类,一个是只有ip这个字段,一个是只有序列号这个字段?
 *       如果这样那就太麻烦了.解决办法在DeviceInfoTwo中
 *  我是为了方便易懂.讲测试的bean分开了两个现实中不需要分开. 直接在上面使用就OK了
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor//无参构造
public class DeviceInfo implements Serializable {

    /**
     * 设备ip
     */
    @NotBlank(message = "ip地址不能为空")
    @Pattern(regexp = Constant.IP_CHECK, message = "IP地址错误")
    private String ip;



    /**设备序列号不能为空*/
    @NotBlank(message = "设备序列号不能为空")
    private String serialNo;








}
