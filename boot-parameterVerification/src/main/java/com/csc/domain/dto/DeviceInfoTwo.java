package com.csc.domain.dto;

import com.csc.Group.FenZuOne;
import com.csc.Group.FenZuTwo;
import com.csc.anno.QsmSpecifiedSelector;
import com.csc.bean.Constant;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * 创建人:csc
 * 时间: 2021-07-07 15:15:00
 * 说明: 该类主要用于测试validation参数校验框架
 * 该类中引入参数校验的另一种需求,就是对一个类中不同参数进行分组校验.
 * 一个接口只需要ip地址必传.另外一个接口只需要序列号
 * 我是为了方便易懂.讲测试的bean分开了两个(DeviceInfoTwo &&　DeviceInfo)现实中不需要分开. 直接在上面使用就OK了
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor//无参构造
public class DeviceInfoTwo {

    /**
     * 设备ip
     * 有的人说了:如果我想两个分类都需要检验IP地址不能为空怎么办?如下把另外一个分组也写上
     * @NotBlank(message = "ip地址不能为空", groups = {FenZuTwo.class})
     */
    //@NotBlank(message = "ip地址不能为空", groups = {FenZuTwo.class})
    @NotBlank(message = "ip地址不能为空", groups = {FenZuOne.class})
    @Pattern(regexp = Constant.IP_CHECK, message = "IP地址错误")
    private String ip;


    @NotBlank(message = "testString的值不能为空")
    @QsmSpecifiedSelector(strValues = {"a","b","c"}, message = "测试值testString指定值必须为a/b/c")
    private String testString;

    @NotNull(message = "testInteger的值不能为空")
    @QsmSpecifiedSelector(intValues = {1,2,3},message = "指定测试值testInteger必须为1/2/3")
    private Integer testInteger;

    /**
     * 设备序列号不能为空
     *  FenZuTwo 只需要校验序列号不能为空
     */
    @NotBlank(message = "ip地址不能为空", groups = {FenZuTwo.class})
    @NotBlank(message = "设备序列号不能为空")
    private String serialNo;

}
