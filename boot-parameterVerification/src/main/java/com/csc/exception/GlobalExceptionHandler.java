package com.csc.exception;


import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.crypto.IllegalBlockSizeException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;
import java.util.List;

/**
 * 作者:csc
 * 时间: 2021-07-07 18:01:00
 * 作用: 自定义全局异常处理类
 * 原来自带的异常信息抛出比较乱.所以捕捉参数异常.进行数据清洗
 * 对@Valid抛出的异常(MethodArgumentNotValidException)进行捕获，并对它重新自定义异常信息

 */
@ControllerAdvice
public class GlobalExceptionHandler {

    /**引入日志*/
    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseBody
    public JSONObject  MyExceptionHandle(MethodArgumentNotValidException exception){
        BindingResult result = exception.getBindingResult();
        JSONObject errorObject = new JSONObject(true);
        if (result.hasErrors()) {
            List<FieldError> fieldErrors = result.getFieldErrors();
            fieldErrors.forEach(error -> {
                errorObject.put("flag",false);
                errorObject.put("msg", error.getDefaultMessage());
            });
        }
        return errorObject;
    }//end MyExceptionHandle


    //实体对象前不加@RequestBody注解,校验方法参数或方法返回值时,未校验通过时抛出的异常
    //Validation-api包里面的异常
    @ExceptionHandler(ValidationException.class)
    public JSONObject methodArguments(ValidationException e){
        JSONObject errorObject = new JSONObject(true);
        errorObject.put("flag",false);
        errorObject.put("msg", e.getCause().getMessage());
        return errorObject;
    }//end


    /**
     * 处理空指针的异常
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value =NullPointerException.class)
    public JSONObject exceptionHandler(HttpServletRequest req, NullPointerException e){
        logger.error("空指针异常"+e);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("flag",false);
        jsonObject.put("msg","啊哦!空指针异常了.请赶紧联系开发人员");
        return jsonObject;
    }//end NullPointerException



}//end GlobalExceptionHandler
