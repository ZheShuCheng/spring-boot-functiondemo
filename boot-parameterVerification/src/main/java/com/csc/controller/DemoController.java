package com.csc.controller;

import com.csc.Group.FenZuOne;
import com.csc.Group.FenZuTwo;
import com.csc.domain.dto.DeviceInfo;
import com.csc.domain.dto.DeviceInfoTwo;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.groups.Default;

/**
 * @Author csc
 * @Date 2021-08-23 16:31
 * @Description: 用户层
 * @Version 1.0
 */
@RestController
public class DemoController {


    /**
     * 验证ip和序列号.
     * @param info
     */
    @PostMapping("/validate")
    public void validate(@Valid @RequestBody DeviceInfo info){
        System.out.println(info);
    }//end

    /**
     * 该方法只效验IP地址
     * @param infoTwo
     */
    @PostMapping("/validate1")
    public void validate1(@Validated({FenZuOne.class, Default.class}) @RequestBody DeviceInfoTwo infoTwo){
        System.out.println(infoTwo);
    }

    /**
     * 该方法只效验序列号
     * @param infoTwo
     */
    @PostMapping("/validate2")
    public void validate2(@Validated({FenZuTwo.class, Default.class}) @RequestBody DeviceInfoTwo infoTwo){
        System.out.println(infoTwo);
    }



}
