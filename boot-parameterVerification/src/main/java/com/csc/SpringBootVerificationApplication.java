package com.csc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author csc
 * @Date 2021-08-23 16:30
 * @Description: 启动器
 * @Version 1.0
 */
@SpringBootApplication
public class SpringBootVerificationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootVerificationApplication.class,args);
    }

}
