模块说明:
 1. 该模块是一个加密工具. 具体作用是讲你模块中引用的jar包.比如你的model,工具类等代码加密. 多个jar包用,分割.命令如下
java -jar classfinal-fatjar-1.2.2.jar -file controller.jar -libjars  qianyi-1.0.jar,common-1.0.jar,huaxia-1.0.jar,jieshun-1.0.jar,niren-relay-1.0.jar,yushi-1.0.jar,zhenshi-1.0.jar -packages com.lgdz -pwd # -Y

2. 加密后运行的时候需要使用如下命令启动jar包:
java -javaagent:<jar包名称> -jar <jar包名称>.jar  

如果大家感兴趣的话:可以自己去搜索一下classFinal加密
推荐:  https://blog.csdn.net/qingquanyingyue/article/details/108475301
我这里只是总结了一下.提供了一个例子而已.

3. 这种加密模式比xjar轻量很多. 并且不会增大jar包运行是所占用的内存