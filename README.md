# spring-boot-functiondemo

#### 介绍
项目说明
 该项目与其它spring-boot-demo不同。是本人将所遇到的需求整合到一起的项目
项目简介
spring-boot-functiondemo 是一个用来深度学习并记录解决的问题的spring boot 的项目.
该项目已成功整合并集成,并且每个模块项目中都有一个read.md文件. 有非常实用的基础知识分享哦

#### 项目环境
 
 JDK1.8
 maven
 springBoot




#### 项目目录介绍

1.集成通过注解方式来进行<接口参数加解密模块=boot-parameterVerification>
  1.1自定义注解指定字段传入的值必须是定义的某些值.
2.<参数校验+分组检验示例>指定参数需要检验的方式  
3.<jar包加密模块>(防止jar包被解压或者反编译工具查看源码和配置信息)  
4.springBoot整合redis.企业级操作工具类
5. 利用class-final进行加密.比xjar要轻盈的多.并且运行也不会占用大量内存去解码 

 持续更新中

#### 使用说明

下载本项目后.每个模块都有启动文件. 直接右键运行即可


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
