package com.csc.netty.client;

import cn.hutool.core.lang.Console;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.IdleStateHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;

/**
 * @author csc
 * @Description: Netty客户端
 * @date 2022/3/1011:48
 */
public class NettyClient {




    static Logger log = LoggerFactory.getLogger(NettyClient.class);



    /**
     * 当前重接次数
     */
    private int reconnectTimes = 1;

    public NettyClient(String host, int port) {
        remoteAddress = new InetSocketAddress(host, port);
    }

    public static void main(String[] args) {

        NettyClient client = new NettyClient("47.108.175.1", 443);
        try {
            client.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    InetSocketAddress remoteAddress;

    public void connect() {
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group)
                    //设置TCP长连接,一般如果两个小时内没有数据的通信时,TCP会自动发送一个活动探测数据报文
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .channel(NioSocketChannel.class)
                    .remoteAddress(remoteAddress)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch){
                            ChannelPipeline pipeline = ch.pipeline();
                            pipeline.addLast(new IdleStateHandler(0, 20, 0));

                        }
                    });
            ChannelFuture f = bootstrap.connect(remoteAddress)
                    .sync();
            f.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Console.log("");
            //          group.shutdownGracefully();  //这里不再是优雅关闭了
            //设置最大重连次数，防止服务端正常关闭导致的空循环
            if (reconnectTimes <= 10) {
                reConnectServer();
            } else {
                log.error("重连次数已经达到上限, 关闭client");
//                group.shutdownGracefully(10L, 10L, TimeUnit.SECONDS);
                group.shutdownGracefully();
            }
        }
    }

    /**
     * 断线重连
     */
    private void reConnectServer() {
        try {
            Thread.sleep(5000);
            log.error("客户端进行断线重连[{}]", reconnectTimes);
            reconnectTimes++;
            connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//end


}
