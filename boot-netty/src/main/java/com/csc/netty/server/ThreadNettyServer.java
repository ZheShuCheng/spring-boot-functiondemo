package com.csc.netty.server;

import com.csc.utils.FixedLengthFrameEncoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;

/**
 * @author ：csc
 * @date ：Created in 2021-10-29 14:11
 * @description：netty服务端
 * @modified By： 启动时不影响其它线程
 * @version: 2.0$
 */
public class ThreadNettyServer {

    //引入日志
    private final static Logger logger = LoggerFactory.getLogger(ThreadNettyServer.class);


    // 指向自己实例的私有静态引用，主动创建
    private static ThreadNettyServer instance;
    //私有的构造方法
    private ThreadNettyServer(){};
    /**
     * 利用双检索
     * @return
     */
    public static ThreadNettyServer getInstance()
    {
        if(instance==null) {//先判断是否为null 后上锁进行初始化
            synchronized (NettyServer.class) {
                if (instance == null)//将对象上锁之后再次判断 是否有别的线程初始化了
                    instance = new ThreadNettyServer();
            }
        }
        return instance;
    }//end


    //成员变量
    private Thread thread;
    public Thread getThread() {
        return thread;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }



    synchronized public void initNettyThread(int port) {
        if (instance.getThread() == null) {
            Thread thread =new Thread(()->{
                try {
                    instance.start(port);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            instance.setThread(thread);
            thread.start();
        }
    }//end initNettyThread








    public void start(int port) throws Exception {
        NioEventLoopGroup group = new NioEventLoopGroup(); //3
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(group)                                //4
                    .channel(NioServerSocketChannel.class)        //5
                    .localAddress(new InetSocketAddress(port))    //6
                    .childHandler(new ChannelInitializer<SocketChannel>() { //7
                        @Override
                        public void initChannel(SocketChannel ch) {
                            logger.info("Netty服务端启动成功端口,9092:"+ch);
                            // 增加解码器，解决Tcp服务服务器粘包/拆包的问题
                            ch.pipeline().addLast(new FixedLengthFrameEncoder(20));
                            // 进行业务处理
                            ch.pipeline().addLast(new NettyServerHandler());
                        }
                    })
                    //服务端接受连接的队列长度，如果队列已满，客户端连接将被拒绝
                    .option(ChannelOption.SO_BACKLOG,10);

            //绑定端口，开始接收进来的连接
            ChannelFuture f = b.bind().sync(); //绑定服务器，等待绑定完成，调用sync()的原因是 当前线程阻塞
            //关闭channel和块，直到它被关闭
            f.channel().closeFuture().sync();
        } finally {
            //关闭EventLoopGroup，释放所有资源(包括所有创建的线程)
            group.shutdownGracefully();
        }
    }









}
