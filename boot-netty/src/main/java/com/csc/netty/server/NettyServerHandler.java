package com.csc.netty.server;

import java.util.HashMap;
import java.util.Map;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;

public class NettyServerHandler extends ChannelInboundHandlerAdapter {

	
	Map<String, ChannelHandlerContext> map = new HashMap<String, ChannelHandlerContext>();
 	
    @Override
    public void channelRead(ChannelHandlerContext ctx,
                            Object msg) {
    	
		/*
		 * ByteBuf in = (ByteBuf) msg; //接受到的数据. String mark =
		 * in.toString(CharsetUtil.UTF_8);
		 * 
		 * if(mark.length()==6) { System.out.println("我进来了"); map.put(mark, ctx); }
		 * System.out.println("Server received: " +
		 * in.toString(CharsetUtil.UTF_8)+"\n"); //2 String remark ="OK";
		 * ctx.write(remark);
		 */                           //3
    	
    	
//    	 ByteBuf byteBuf = (ByteBuf) msg;
//    	    byte[] content = new byte[byteBuf.readableBytes()];
//    	    byteBuf.readBytes(content);
//    	    System.out.println(Thread.currentThread() + ": 最终打印" + new String(content));
//    	    //((ByteBuf) msg).release();
    	
    	ByteBuf in = (ByteBuf) msg;
		// 接受到的数据.
		String mark = in.toString(CharsetUtil.UTF_8);
		System.out.println("Server received: " + mark + "\n"); // 2
    	
        
    }

	/*
	 * //数据读取完毕事件
	 * 
	 * @Override public void channelReadComplete(ChannelHandlerContext ctx) throws
	 * Exception { ctx.writeAndFlush(Unpooled.EMPTY_BUFFER)//4
	 * .addListener(ChannelFutureListener.CLOSE); String msg = "OK"; ctx.write(msg);
	 * 
	 * }
	 */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx,
                                Throwable cause) {
        cause.printStackTrace();                //5
        ctx.close();                            //6
    }
}

