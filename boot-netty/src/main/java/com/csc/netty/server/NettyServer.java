package com.csc.netty.server;

import java.net.InetSocketAddress;


import com.csc.utils.FixedLengthFrameEncoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @Author csc
 * netty服务端
 */
public class NettyServer {

    /**
     * 引入日志
     */
    private static final Logger logger = LoggerFactory.getLogger(NettyServer.class);

    public final int port;

    /**
     * 初始化方法
     *
     * @param port
     */
    public NettyServer(int port) {
        this.port = port;
    }

    /**
     * 启动方法
     *
     * @throws Exception
     */
    public void init() throws Exception {
        NioEventLoopGroup group = new NioEventLoopGroup(); //3
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(group)                                //4
                    .channel(NioServerSocketChannel.class)        //5
                    .localAddress(new InetSocketAddress(port))    //6
                    .childHandler(new ChannelInitializer<SocketChannel>() { //7
                        @Override
                        public void initChannel(SocketChannel ch) {

                            logger.info("initChannel ch:" + ch);

                            // 增加解码器,解决粘包问题
                            ch.pipeline().addLast(new FixedLengthFrameEncoder(20));
                            ch.pipeline().addLast(new NettyServerHandler());
                        }
                    });
            ChannelFuture f = b.bind().sync();            //8
            logger.info(NettyServer.class.getName() +
                    " started and listen on " + f.channel().localAddress());
            f.channel().closeFuture().sync();            //9
        } finally {
            group.shutdownGracefully().sync();            //10
        }
    }//init

}
