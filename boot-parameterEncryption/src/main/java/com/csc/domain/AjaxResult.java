package com.csc.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 该类主要用于定义.返回结果
 */
@Data
@NoArgsConstructor//无参构造
@AllArgsConstructor//有参构造
@Accessors(chain = true)//开启链式写法.set().set()
public class AjaxResult<T> implements Serializable {

    /**true是成功 false失败*/
    private boolean flag;

    /**登录提示信息*/
    private String msg;

    /**数据*/
    private Object data;


    /**
     * 成功方法
     * @param data
     * @param <R>
     */
    public static <R>  AjaxResult success(R data) {
        return new AjaxResult(true, "success", data);
    }

}
