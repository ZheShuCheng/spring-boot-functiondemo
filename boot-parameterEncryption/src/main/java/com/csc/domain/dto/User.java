package com.csc.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author csc
 * @Date 2021-08-23 14:30
 * @Description: 测试实体类
 * @Version 1.0
 */
@Data
public class User {

    private String username;

    private String password;




}
