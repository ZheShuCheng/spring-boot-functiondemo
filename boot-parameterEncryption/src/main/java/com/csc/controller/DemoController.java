package com.csc.controller;

import cn.hutool.core.codec.Base64;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSONObject;
import com.csc.anno.SafetyProcess;
import com.csc.domain.AjaxResult;
import com.csc.domain.dto.User;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


/**
 * @Author csc
 * @Description
 * @Date 2021/08/23 14:01
 */
@RestController
public class DemoController {

    /**
     * 请求参数解密，响应结果加密
     * @return
     */
    @SafetyProcess
    @PostMapping(value = "/test")
    public AjaxResult test(@RequestBody User user) {
        return AjaxResult.success(user);
    }






}
