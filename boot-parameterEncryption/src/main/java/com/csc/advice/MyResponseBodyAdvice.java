package com.csc.advice;

import cn.hutool.core.codec.Base64;
import cn.hutool.crypto.SecureUtil;
import com.csc.anno.SafetyProcess;
import com.csc.domain.other.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;


/**
 * @Author csc
 * @Description
 * @Date 2021-07-15
 */
@ControllerAdvice
@Slf4j
public class MyResponseBodyAdvice implements ResponseBodyAdvice {


    @Value("${safety.secret}")
    private String secret;

    @Override
    public boolean supports(MethodParameter methodParameter, Class aClass) {
        SafetyProcess process = methodParameter.getMethodAnnotation(SafetyProcess.class);
        //如果带有安全注解且标记为加密，测进行加密操作
        return null != process && process.encode();
    }

    /**
     * 约定，响应方加密算法：
     * 1.对源数据转换成json字符串
     * 2.然后调用aes加密
     * 3.将加密后的数据进行base64编码
     * @param o
     * @param methodParameter
     * @param mediaType
     * @param aClass
     * @param serverHttpRequest
     * @param serverHttpResponse
     * @return
     */
    @Override
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType, Class aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        Object data = null;
        //如果是rest接口统一封装返回对象
        if (o instanceof Response) {
            Response res = (Response) o;
            //如果返回成功
            if (res.isOk()) {
                data = res.getData();
            } else {
                return o;
            }
        } else {
            data = o;
        }
        if (null != data) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                String bodyStr = objectMapper.writeValueAsString(o);

                //aes加密
                byte[] bytes = SecureUtil.aes(secret.getBytes()).encrypt(bodyStr);
                //base64编码返回
                return Base64.encode(bytes);
            } catch (JsonProcessingException e) {
                log.error("数据加密异常", e);
            }
        }
        return o;
    }//end  beforeBodyWrite


}
