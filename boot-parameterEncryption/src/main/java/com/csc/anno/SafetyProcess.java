package com.csc.anno;

import java.lang.annotation.*;

/**
 * @Author csc
 * @Description: 自定义注解.用于参数的加解密
 * @Date 2021-07-15
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface SafetyProcess {

    /**
     * 请求参数是否解密
     * 默认true解密
     */
    boolean decode() default true;

    /**
     * 响应结果是否加密
     * 默认true加密
     */
    boolean encode() default true;

}
