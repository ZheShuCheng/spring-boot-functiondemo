package com.csc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author csc
 * @Date 2021-08-23 11:04
 * @Description: 启动器
 * @Version 1.0
 */
@SpringBootApplication
public class SpringBootParameterEncryptionApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootParameterEncryptionApplication.class, args);
    }

}
