package com.csc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author csc
 * @Date 2021-08-28 9:55
 * @Description: 启动器
 * @Version 1.0 我提交一下
 */
@SpringBootApplication
public class SpringBootRedisApplication {


    public static void main(String[] args) {
      SpringApplication.run(SpringBootRedisApplication.class,args);
    }





}
