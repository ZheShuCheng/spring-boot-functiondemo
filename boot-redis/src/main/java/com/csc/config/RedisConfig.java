package com.csc.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @description: Redis配置类
 * @author: ydf
 * @date: 2020/12/25 21:45
 * @version: v1.0
 */
@Configuration
public class RedisConfig {

    /**
     * 主题名称（频道名称）
     */
    public static final String TOPIC_NAME = "topicName";

    /**
     * 自定义序列化机制
     * @param connectionFactory
     * @return
     */
    @Bean
    @SuppressWarnings("all")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
        template.setConnectionFactory(connectionFactory);
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        // key采用String的序列化方式
        template.setKeySerializer(stringRedisSerializer);
        // hash的key也采用String的序列化方式
        template.setHashKeySerializer(stringRedisSerializer);
        // value序列化方式采用jackson
        template.setValueSerializer(jackson2JsonRedisSerializer);
        // hash的value序列化方式采用jackson
        template.setHashValueSerializer(jackson2JsonRedisSerializer);
        template.afterPropertiesSet();
        return template;
    }

    /**
     * 配置Redis消息的监听容器
     * @param connectionFactory
     * @param messageListenerAdapter
     * @return
     */
	/*
	 * @Bean
	 *
	 * @SuppressWarnings("all") RedisMessageListenerContainer
	 * container(RedisConnectionFactory connectionFactory,MessageListenerAdapter
	 * messageListenerAdapter) { RedisMessageListenerContainer container = new
	 * RedisMessageListenerContainer();
	 * container.setConnectionFactory(connectionFactory); // 配置主题名称：topicName
	 * container.addMessageListener(messageListenerAdapter, new
	 * PatternTopic(TOPIC_NAME)); return container; }
	 */

}
