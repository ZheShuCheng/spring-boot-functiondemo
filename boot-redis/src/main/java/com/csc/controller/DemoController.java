package com.csc.controller;

import com.alibaba.fastjson.JSONPObject;
import com.csc.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author csc
 * @Date 2021-08-28 10:51
 * @Description: (用一句话描述该文件做什么)
 * @Version 1.0
 */
@RestController
public class DemoController {

    /**引入redisUtil*/
    @Autowired
    private RedisUtil redisUtil;


    /**
     * 测试是否连接成功redis
     * 放入数据后. 可登录redis客户端(RedisDesktopManager)查看.
     * 十秒钟后数据消失
     * @param receive
     * @return
     */
    @PostMapping("/add")
    public boolean add(JSONPObject receive){
        //讲数据放入十秒钟
      boolean flags =  redisUtil.set("ceshi",receive.toJSONString(),10);
      return flags;
    }



}
